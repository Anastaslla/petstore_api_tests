users_data = (({
  "id": 1001,
  "username": "theUser1",
  "firstName": "John",
  "lastName": "James",
  "email": "john@email.com",
  "password": "12345",
  "phone": "12345",
  "userStatus": 1
}),
({
  "id": 1002,
  "username": "theUser2",
  "firstName": "Anne",
  "lastName": "Nikolas",
  "email": "anne@email.com",
  "password": "qwerty",
  "phone": "222333555",
  "userStatus": 0
}))

update_users_data = (("theUser1", {
  "id": 1001,
  "username": "theUser1changed",
  "firstName": "Johnchanged",
  "lastName": "Jameschanged",
  "email": "john@email.comchanged",
  "password": "12345changed",
  "phone": "12345changed",
  "userStatus": 2
}),)
