from api_clients.abstract_client import AbstractClient
from test_data.user_login_credentials import credits


class UserClient(AbstractClient):
    class User:
        user_login_text = "Logged in user session"

    def user_login(self):
        return self.get("/user/login", **credits)

    def create_user(self, user_data: dict):
        return self.post("/user", user_data)

    def update_user(self, user, update_data: dict):
        end_point = f"/user/{user}"
        return self.put(end_point, update_data)
