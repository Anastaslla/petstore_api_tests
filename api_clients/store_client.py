from enum import Enum

from api_clients.abstract_client import AbstractClient


class StoreClient(AbstractClient):
    class InventoryStatus(Enum):
        approved = "approved"
        placed = "placed"
        delivered = "delivered"

        def __str__(self):
            return self.value

    def get_inventory(self):
        return self.get("/store/inventory")

    def store_order(self, order: dict):
        return self.post("/store/order", order)

    def delete_store_order(self, order_id: int):
        end_point = f"/store/order/{order_id}"
        return self.delete(end_point)
