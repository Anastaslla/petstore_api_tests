from abc import ABC
import requests


class AbstractClient(ABC):
    def __init__(self):
        self.__api_session = requests.Session()
        self.__api_session.headers.update(
            {
                'accept': 'application/json',
                'Content-Type': 'application/json'
            }

        )
        self.__url = "https://petstore3.swagger.io/api/v3"

    def __form_url(self, endpoint: str):
        return self.__url + endpoint

    def get(self, endpoint: str, **kwargs):
        return self.__api_session.get(self.__form_url(endpoint), params=kwargs)

    def post(self, endpoint: str, request_body: dict=None):
        request_body = dict() if not request_body else request_body
        return self.__api_session.post(self.__form_url(endpoint), json=request_body)

    def put(self, endpoint: str, request_body: dict=None):
        request_body = dict() if not request_body else request_body
        return self.__api_session.put(self.__form_url(endpoint), json=request_body)

    def delete(self, endpoint: str, **kwargs):
        return self.__api_session.delete(self.__form_url(endpoint), params=kwargs)

    def session_close(self):
        self.__api_session.close()

    def __del__(self):
        self.session_close()
