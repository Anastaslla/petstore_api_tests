import pytest
import json
from api_clients.user_client import UserClient
from test_data.users_test_data import users_data, update_users_data


@pytest.mark.user_api
def test__user_login(user_client: UserClient):
    user_login_request = user_client.user_login()

    assert user_login_request.ok, f"Request finished unnormally with status {user_login_request.status_code}"
    assert UserClient.User.user_login_text in user_login_request.text, f"{UserClient.User.user_login_text} is not present in response. Response body:\n {user_login_request.text}"


@pytest.mark.user_api
@pytest.mark.parametrize("test_data", (users_data))
def test_user_creation(user_client: UserClient, test_data):
    create_user_request = user_client.create_user(test_data)

    assert create_user_request.ok, f"Request finished unnormally with status {create_user_request.status_code}"
    assert create_user_request.json() == test_data, f"incorrect response {create_user_request.text} should be:\n {json.dumps(test_data)}"


@pytest.mark.user_api
@pytest.mark.parametrize("username, update_data", (update_users_data))
def test_user_update(user_client: UserClient, username, update_data):
    update_user_request = user_client.update_user(username, update_data)

    assert update_user_request.ok, f"Request finished unnormally with status {update_user_request.status_code}"
    assert update_user_request.json() == update_data, f"incorrect response {update_user_request.text} should be:\n {json.dumps(update_data)}"
