import json

import pytest
import pytest_check as check

import test_data.orders_test_data as test_data
from api_clients.store_client import StoreClient


@pytest.mark.simple_store
def test_store_inventory(store_client: StoreClient):
    inventory_list = store_client.get_inventory()

    assert inventory_list.ok, f"Request finished unnormally with status {inventory_list.status_code}"

    for status in store_client.InventoryStatus.__members__.values():
        response_body = inventory_list.json()
        check.is_in(str(status), response_body.keys(), f"status {str(status)} is not present in response. Response body:\n {inventory_list.text}")
        check.is_true(isinstance(response_body[str(status)], int), f"response_body[str(status)] is not an integer. Response body:\n {inventory_list.text}")
        check.is_true(response_body[str(status)] >= 0,  f"response_body[str(status)] is not . Response body:\n {inventory_list.text}")


@pytest.mark.simple_store
@pytest.mark.parametrize("test_data", (test_data.test_orders1))
def test_store_order(store_client: StoreClient, test_data: dict):
    result = store_client.store_order(test_data)

    assert result.ok, f"Request finished unnormally with status {result.status_code}"
    assert result.json() == test_data, f"incorrect response {result.text} should be:\n {json.dumps(test_data)}"


@pytest.mark.simple_store
@pytest.mark.parametrize("test_data",(test_data.test_orders1))
def test_delete_order(store_client: StoreClient, test_data: dict):
    _ = store_client.store_order(test_data)

    deleted_order = store_client.delete_store_order(test_data["id"])

    assert deleted_order.ok, f"Request finished unnormally with status {deleted_order.status_code}"
