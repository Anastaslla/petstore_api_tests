import pytest

from api_clients.store_client import StoreClient
from api_clients.user_client import UserClient


@pytest.fixture
def store_client():
    store_client = StoreClient()

    yield store_client

    store_client.session_close()

@pytest.fixture
def user_client():
    user_client = UserClient()

    yield user_client

    user_client.session_close()